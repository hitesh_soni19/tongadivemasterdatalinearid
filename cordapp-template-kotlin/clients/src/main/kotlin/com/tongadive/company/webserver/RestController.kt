package com.tongadive.company.webserver

import com.tongadive.company.flows.CreateCompanyFlow
import com.tongadive.company.flows.UpdateCompanyFlow
import com.tongadive.company.model.Company
import com.tongadive.company.states.CompanyState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.transactions.SignedTransaction
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import java.util.*

/**
 *  This Api is use to perform CRUD operation for company in Corda
 *  EndpointBaseURl - /api/masterData
 */
@RestController
@RequestMapping("/api/masterData") // The paths for HTTP requests are relative to this base path.
class RestController(rpc: NodeRPCConnection) {

    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    private val proxy = rpc.proxy

    /**
     * EndPoint - /test
     * @return Master node is running as string
     * This EndPoint will test if the node is running or not
     */
    @GetMapping(value = ["/health"], produces = ["text/plain"])
    private fun test(): String { //health
        return "Master node is running"
    }
    /**
     * EndPoint - /companies
     * @param   - Company Object as JSON
     * @return - Company ID : 30f073ee-4769-4e42-b3e5-1aa14cdeb368 created successfully
     * This EndPoint will create a company record on ledger and return linear ID of the created company record
     */
    @PostMapping(
        value = "/companies",
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    @Throws(
        IllegalArgumentException::class
    )
    private fun createCompany(@RequestBody company: Company): ResponseEntity<String> {
        return try {
            val result: SignedTransaction =
                proxy.startTrackedFlowDynamic(CreateCompanyFlow::class.java, company).returnValue.get()
            // Return the response.
            ResponseEntity
                .status(HttpStatus.CREATED)
                .body(
                    "Company ID : " + result.tx.outputsOfType(CompanyState::class.java)[0].linearId
                        .id.toString().toString() + " created successfully"
                )
        } catch (e: Exception) {
            ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.message)
        }
    }

    /**
     * EndPoint - /companies/{companyID}
     * @param   - Company ID, value, primary
     * @return - Company - 30f073ee-4769-4e42-b3e5-1aa14cdeb368 is updated successfully
     * This EndPoint will update a company record on ledger and return linear ID of the updated company record
     */
    @PutMapping(value = "/companies/{companyID}", produces = [MediaType.APPLICATION_JSON_VALUE])
    @Throws(
        IllegalArgumentException::class
    )
    private fun updateCompany(
        @PathVariable companyID: String?,
        @RequestParam value: String,
        @RequestParam primary: String
    ): ResponseEntity<String> {
        if (companyID == null) return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body("Please provide Company ID")
        val id = UniqueIdentifier(null, UUID.fromString(companyID))
        return try {
            val result: SignedTransaction? =
                proxy.startTrackedFlowDynamic(UpdateCompanyFlow::class.java, id, value, primary).returnValue.get()
            ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Company - " + id.toString().toString() + " is updated successfully")
        } catch (e: Exception) {
            ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.message)
        }
    }
}
