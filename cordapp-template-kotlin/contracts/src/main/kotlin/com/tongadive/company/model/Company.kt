package com.tongadive.company.model

import net.corda.core.serialization.ConstructorForDeserialization
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
class Company {
    var companyName: String? = null
        private set
    var identifierType: String? = null
        private set
    var value: String? = null
        private set
    var primary: String? = null
        private set

    @ConstructorForDeserialization
    constructor(companyName: String?, identifierType: String?, value: String?, primary: String?) {
        this.companyName = companyName
        this.identifierType = identifierType
        this.value = value
        this.primary = primary
    }

    override fun toString(): String {
        return "Company(companyName=$companyName, identifierType=$identifierType, value=$value, primary=$primary)"
    }
}