package com.tongadive.company.schema

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.serialization.CordaSerializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

/**
* The family of schemas for CompanySchema.
*/
object CompanySchema

/**
 * An CompanySchemaV1 schema.
 */

@CordaSerializable
object CompanySchemaV1 : MappedSchema(
    schemaFamily = CompanySchema.javaClass,
    version = 1,
    mappedTypes = listOf (PersistentCompany::class.java)) {
    /**
     *  This will create a table in database name with company
     */
    @Entity
    @Table(name = "company")
    class PersistentCompany(

        @Column(name = "companyName")
        val companyName: String,

        @Column(name = "identifierType")
        val identifierType: String,

        @Column(name = "value")
        val value: String,

        @Column(name = "primaryValue")
        val primaryValue: String,

        @Column(name = "linearID")
        val linearID: UUID

    ): PersistentState() {
        // Default constructor required by hibernate.
        constructor() : this("","","","", UUID.randomUUID())
    }
}