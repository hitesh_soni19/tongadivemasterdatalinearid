package com.tongadive.company.contracts

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.transactions.LedgerTransaction

import net.corda.core.contracts.requireThat

class GeneralContract:Contract {

    companion object {
        // This is used to identify our contract when building a transaction.
        val ID = "com.tongadive.company.contracts.GeneralContract"
    }

    /**
     * The verify() function of all the states' contracts must not throw an exception for a transaction to be
     * considered valid.
     */
    override fun verify(tx:LedgerTransaction) {

         if(tx.commands.size !==1){
             throw IllegalArgumentException("One command is required")
         }

        val command = tx.commands.single().value

        if(command is Commands.Create){
            requireThat {
                "A product create transaction shouldn't consume any input state".using(tx.inputs.isEmpty());
                "A product create transaction should produce one output state".using (tx.outputs.size == 1)
            }
        }
        else if (command is Commands.Update) {
            requireThat {
                "A product update transaction should consume one input state".using(tx.inputs.size==1);
                "A product update transaction should produce one output state".using (tx.outputs.size == 1)
            }
        }
        else if (command is Commands.Delete){
            requireThat {
                "A product delete transaction should consume one input state".using(tx.inputs.size==1);
                "A product delete transaction should produce one output state".using (tx.outputs.size == 1)
            }
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands:CommandData {
        class Create: Commands
        class Update: Commands
        class Delete: Commands
    }
}