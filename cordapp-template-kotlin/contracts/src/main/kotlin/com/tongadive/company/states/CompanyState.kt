package com.tongadive.company.states

import com.tongadive.company.contracts.GeneralContract
import com.tongadive.company.model.Company
import com.tongadive.company.schema.CompanySchemaV1
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.ConstructorForDeserialization
import net.corda.core.serialization.CordaSerializable
import java.lang.IllegalArgumentException

/**
 * This states plays role of company on ledger.
 */
@BelongsToContract(GeneralContract::class)
class CompanyState : LinearState,QueryableState {

    override val linearId:UniqueIdentifier
    private val company: Company
    private val itemManufacturer:Party


    constructor(company: Company, itemManufacturer:Party) {
        this.linearId = UniqueIdentifier()
        this.company = company
        this.itemManufacturer = itemManufacturer
    }

    @ConstructorForDeserialization
    constructor(linearId:UniqueIdentifier, company: Company, itemManufacturer:Party) {
        this.linearId = linearId
        this.company = company
        this.itemManufacturer = itemManufacturer
    }

    override val participants:List<AbstractParty>
        get() {
            return listOf<AbstractParty>(itemManufacturer)
        }

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return if (schema is CompanySchemaV1) {
            CompanySchemaV1.PersistentCompany(
                this.company.companyName.toString(),
                this.company.identifierType.toString(),
                this.company.value.toString(),
                this.company.primary.toString(),
                this.linearId.id
            )
        } else {
            throw IllegalArgumentException("Unrecognised schema \$schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> {
        return listOf(CompanySchemaV1)
    }

    fun getCompany(): Company {
        return company
    }

    fun getItemManufacturer(): Party {
        return itemManufacturer
    }
}