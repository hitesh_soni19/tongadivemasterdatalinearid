package com.tongadive.company.flows

import co.paralleluniverse.fibers.Suspendable
import com.tongadive.company.contracts.GeneralContract
import com.tongadive.company.model.Company
import com.tongadive.company.states.CompanyState
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class CreateCompanyFlow(private val company: Company) : FlowLogic<SignedTransaction>() {

    @Suspendable
    @Override
    @Throws(FlowException::class)
    override fun call(): SignedTransaction {

        // Obtain a reference from a notary
        /**
         *  METHOD 1: Take first notary on network, WARNING: use for test, non-prod environments, and single-notary networks only!*
         *  METHOD 2: Explicit selection of notary by CordaX500Name - argument can by coded in flow or parsed from config (Preferred)
         *
         *  * - For production you always want to use Method 2 as it guarantees the expected notary is returned.
         */
        val notary = serviceHub.networkMapCache.notaryIdentities[0] // METHOD 1
       // val notary = serviceHub.networkMapCache.getNotary(CordaX500Name.parse("O=Notary,L=London,C=GB")) // METHOD 2

        val companyState = CompanyState(company, ourIdentity)

        val command = Command(GeneralContract.Commands.Create(), listOf(ourIdentity.owningKey))

        // Build the transaction. On successful completion of the transaction the company state will be unconsumed
        val transactionBuilder = TransactionBuilder(notary)
            .addCommand(command)
            .addOutputState(companyState, GeneralContract.ID)

        // Verify the transaction
        transactionBuilder.verify(serviceHub)

        // Sign the transaction
        val signedTransaction = serviceHub.signInitialTransaction(transactionBuilder)

        return subFlow(FinalityFlow(signedTransaction, emptyList()))
    }
}