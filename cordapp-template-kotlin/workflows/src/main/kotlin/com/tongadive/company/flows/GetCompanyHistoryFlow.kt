package com.tongadive.company.flows

import co.paralleluniverse.fibers.Suspendable
import com.tongadive.company.model.Company
import com.tongadive.company.schema.CompanySchemaV1
import com.tongadive.company.states.CompanyState
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.FlowException
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.node.services.vault.QueryCriteria
import java.util.ArrayList

@InitiatingFlow
@StartableByRPC
class GetCompanyHistoryFlow(private val companyName: String) : FlowLogic<List<Company>>() {

    @Suspendable
    @Override
    @Throws(FlowException::class)
    override fun call(): List<Company> {

        val companyList = ArrayList<Company>()
        var id = CompanySchemaV1.PersistentCompany::companyName.equal(companyName)
        val query1: QueryCriteria = QueryCriteria.VaultCustomQueryCriteria(id, Vault.StateStatus.ALL)
        val results: Vault.Page<CompanyState> = serviceHub.vaultService.queryBy(CompanyState::class.java, query1)
        var inputStateAndRef: StateAndRef<*>
        println("Size : " + results.states.size)
        for (i in results.states.indices) {
            inputStateAndRef = results.states[i]
            val inputState: CompanyState = inputStateAndRef.state.data
            val obj: Company = inputState.getCompany()
            companyList.add(obj)
        }
        return companyList
    }
}