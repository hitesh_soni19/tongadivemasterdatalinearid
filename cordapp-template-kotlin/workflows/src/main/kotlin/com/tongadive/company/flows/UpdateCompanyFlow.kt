package com.tongadive.company.flows

import co.paralleluniverse.fibers.Suspendable
import com.tongadive.company.contracts.GeneralContract
import com.tongadive.company.model.Company
import com.tongadive.company.states.CompanyState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.flows.FinalityFlow

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class UpdateCompanyFlow(private val linearId: UniqueIdentifier, private val value: String, private val  primary: String)
    :  FlowLogic<SignedTransaction>() {

    @Suspendable
    @Override
    @Throws(FlowException::class)
    override fun call(): SignedTransaction {

        val notary: Party = serviceHub.networkMapCache.notaryIdentities[0]

        val id = arrayListOf<UniqueIdentifier>()
        id.add(linearId)

        val criteria: QueryCriteria = QueryCriteria.LinearStateQueryCriteria(null, id)

        val result: Vault.Page<CompanyState> = serviceHub.vaultService.queryBy(CompanyState::class.java, criteria)

        val inputState = result.states[0]

        val state = inputState.state.data

        val companyName: String? = state.getCompany().companyName
        val identifierType: String? = state.getCompany().identifierType
        //val value: String? = state.getCompany()?.value
        //val primary: String? = state.getCompany()?.primary

        val company = Company(companyName, identifierType, value, primary)
        val companyState = CompanyState(linearId, company, ourIdentity)

        val command: Command<GeneralContract.Commands.Update> =
            Command(GeneralContract.Commands.Update(), listOf(ourIdentity.owningKey))

        val tx: TransactionBuilder = TransactionBuilder(notary)
            .addCommand(command)
            .addInputState(inputState)
            .addOutputState(companyState, GeneralContract.ID)

        tx.verify(serviceHub)

        // Sign the transaction
        val stx: SignedTransaction = serviceHub.signInitialTransaction(tx)

        return subFlow(FinalityFlow(stx, emptyList()))
    }
}