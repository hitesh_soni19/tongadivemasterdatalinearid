package com.tongadive.company.flows

import co.paralleluniverse.fibers.Suspendable
import com.tongadive.company.contracts.GeneralContract
import com.tongadive.company.model.Company
import com.tongadive.company.states.CompanyState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class DeleteCompanyFlow(private val linearId : UniqueIdentifier) : FlowLogic<SignedTransaction>() {

        @Suspendable
        @Override
        @Throws(FlowException::class)
        override fun call(): SignedTransaction {
            // Obtain a reference from a notary
            /**
             *  METHOD 1: Take first notary on network, WARNING: use for test, non-prod environments, and single-notary networks only!*
             *  METHOD 2: Explicit selection of notary by CordaX500Name - argument can by coded in flow or parsed from config (Preferred)
             *
             *  * - For production you always want to use Method 2 as it guarantees the expected notary is returned.
             */
            val notary: Party = serviceHub.networkMapCache.notaryIdentities[0]
            // val notary = serviceHub.networkMapCache.getNotary(CordaX500Name.parse("O=Notary,L=London,C=GB")) // METHOD 2
            val id = arrayListOf<UniqueIdentifier>()
            id.add(linearId)

            val criteria: QueryCriteria = QueryCriteria.LinearStateQueryCriteria(null, id)

            val result: Vault.Page<CompanyState> = serviceHub.vaultService.queryBy(CompanyState::class.java, criteria)

            val inputState: StateAndRef<CompanyState> = result.states[0]

            val company = Company(String(), String(), String(), String())

            val companyState = CompanyState(linearId, company,ourIdentity)

            val command: Command<GeneralContract.Commands.Delete> = Command(GeneralContract.Commands.Delete(), listOf(ourIdentity.owningKey))

            // Build the transaction. On successful completion of the transaction the company state will be unconsumed
            val tx: TransactionBuilder = TransactionBuilder(notary)
                .addCommand(command)
                .addInputState(inputState)
                .addOutputState(companyState, GeneralContract.ID)

            // Verify the transaction
            tx.verify(serviceHub)
            // Sign the transaction
            val stx: SignedTransaction = serviceHub.signInitialTransaction(tx)

            return subFlow(FinalityFlow(stx, emptyList()))

        }
}